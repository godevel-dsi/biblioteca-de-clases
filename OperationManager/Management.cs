﻿namespace OperationManager
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Runtime.InteropServices;

    using a3ERPActiveX;
    using ADODB;
    using System.Windows.Forms;
    using System.Configuration;
    using System.Data;
    using System.Globalization;

    [ClassInterface(ClassInterfaceType.AutoDual), ComVisible(true)]
    [Guid("134FC3FF-9FBB-43CA-AD82-69FD7BD12D55")]
    public class Management
    {
        public Connection Connection { get; private set; }
        public Connection SystemConnection { get; private set; }

        public Enlace enlace { get; set; }

        private string DbContext = "TEST";

        /// <summary>
        /// Para ser invocado desde el menu, programas externos, carga automática desde el erp (tabla dlls)
        /// </summary>
        /// <returns></returns>
        public object[] ListaProcedimientos()
        {
            object[] result = { "INICIAR", "FINALIZAR", "ANTESDEGUARDARDOCUMENTOV2" };

            return result;
        }

        public void Iniciar(string connEmpresaNexus, string connSistemaNexus)
        {
            enlace = new Enlace();
            enlace.Iniciar("");

            string conexioncompleta = Conexion.ConexionSql.GenerarConexion(enlace.Conexion, enlace.Usuario, "A3software");
            Conexion.ConexionSql.AbrirConexion(conexioncompleta);
        }

        public void Finalizar()
        {
            Conexion.ConexionSql.CerrarConexion();
            enlace.Acabar();
        }

        public bool AntesDeGuardarDocumentoV2(string documento, double idDoc, ref object cabecera, ref object lineas, int estado)
        {
            // Detectamos si estamos borrando documento
            try
            {
                StringBuilder sb = new StringBuilder();
                // Si cambian pedido
                if (documento == "AV")
                {
                    switch (estado)
                    {
                        case 1:
                            object[] line = ((lineas as object[])[1] as object[]);

                            DateTime checkin = DateTime.ParseExact(GetValor(line, "PARAM1").ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
                            DateTime checkout = DateTime.ParseExact(GetValor(line, "PARAM2").ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
                            string id = GetValor(line, "IDPEDV").ToString();
                            // Obtenemos total 
                            double total = Convert.ToDouble(GetValor(line, "BASEMONEDAMASIVA"));

                            // Actualizamos linea de reserva
                            sb.Append("UPDATE [" + this.DbContext + "].[dbo].[INFORESERVA] ");
                            sb.Append("SET [INRE_FECENTRADA] = '" + checkin.ToShortDateString() + "'");
                            sb.Append(", [INRE_FECSALIDA] = '" + checkout.ToShortDateString() + "' ");
                            sb.Append(", [INRE_IMPORTERESERVA] = " + total.ToString().Replace(',', '.') + "+ [INRE_IMPORTEANTICIPO] ");
                            sb.Append("WHERE [INRE_IDPEDVENTA] = " + id + ";");

                            Conexion.ConexionSql.Ejecutar(sb.ToString());
                            sb = new StringBuilder();

                            object[] header = ((cabecera as object[])[1] as object[]);
                            string cancelled = GetValor(header, "PARAM1").ToString().ToUpper();
                            bool isCancelled = cancelled.Equals("SÍ") || cancelled.Equals("SI");

                            sb.Append("UPDATE [" + this.DbContext + "].[dbo].[INFORESERVA] ");
                            sb.Append("SET [INRE_FACTCERRADA] = " + (isCancelled ? "1" : "0") + " ");
                            sb.Append("WHERE [INRE_IDPEDVENTA] = " + id + ";");
                            Conexion.ConexionSql.Ejecutar(sb.ToString());
                            break;
                    }
                }
                if (documento == "PV")
                {
                    switch (estado)
                    {
                        case 2:
                            sb.Append("DELETE FROM [" + this.DbContext + "].[dbo].[INFORESERVA] ");
                            sb.Append("WHERE [INRE_IDPEDVENTA] = " + idDoc + ";");
                            Conexion.ConexionSql.Ejecutar(sb.ToString());
                            break;
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un erro realizando la operación. Mensaje de error: " + ex.Message.ToString());
                return false;
            }
            return true;
        }


        private object GetValor(object[] Datos, string Campo)
        {
            object result = null;
            for (int i = 1; (i <= (int)Datos[0]); i++)
            {
                object[] item = (Datos[i] as object[]);
                if (item[0].ToString().ToUpper() == Campo.ToUpper())
                {
                    result = item[1];
                }
            }
            return result;
        }


    }
}
